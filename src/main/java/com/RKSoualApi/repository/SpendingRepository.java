package com.RKSoualApi.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.RKSoualApi.model.Spending;


public interface SpendingRepository extends JpaRepository<Spending, Long>{
	
	List<Spending> findAllByCreatedDateBetweenOrderByCreatedDateDesc(
		      Date start,
		      Date end);
	
	List<Spending> findByCategoryNameOrderByCreatedDateDesc(String name);
	
	@Query(value = "SELECT SUM(amount) from Spending u where u.createdDate >= :startDate and u.createdDate < :endDate ")
	public Double getTotalSpendings(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
}

