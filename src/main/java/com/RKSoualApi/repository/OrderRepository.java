package com.RKSoualApi.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.RKSoualApi.model.Guest;
import com.RKSoualApi.model.Orders;


public interface OrderRepository extends JpaRepository<Orders, Long>{
	
	List<Orders> findByGuestIdOrderByCreatedDateDesc(Long id);
	List<Orders> findAllByCreatedDateBetweenOrderByCreatedDateDesc(
		      Date start,
		      Date end);
	
	List<Orders> findAllByOrderByCreatedDateDesc();
	@Query(value = "SELECT COUNT(u) from Orders u where u.createdDate >= :startDate and u.createdDate < :endDate ")
	public Long getNumberOfAllOrders(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
	
	@Query(value = "SELECT * from orders u where u.money_still > 0", nativeQuery = true)
	public List<Orders> getOrdersWithMonyStill();
	
	@Query(value = "SELECT SUM(moneyStill) from Orders u where u.createdDate >= :startDate and u.createdDate < :endDate ")
	public Long getSumOfMoneyStill(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
	@Query(value = "SELECT SUM(moneyPaid) from Orders u where u.createdDate >= :startDate and u.createdDate < :endDate ")
	public Long getSumOfMoneyPaid(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
	
}
