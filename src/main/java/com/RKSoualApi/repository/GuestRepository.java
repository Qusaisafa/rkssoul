package com.RKSoualApi.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.RKSoualApi.model.Guest;


public interface GuestRepository extends JpaRepository<Guest, Long>{
	Guest findByName(String name);
	
	List<Guest> findAllByOrderByCreatedDateDesc();
	@Query(value = "SELECT COUNT(u) from Guest u where u.createdDate >= :startDate and u.createdDate < :endDate ")
	public Long getNumberOfAllGuests(@Param("startDate")Date startDate,@Param("endDate")Date endDate);

}
