package com.RKSoualApi.repository;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.RKSoualApi.model.OrdersMedicine;


public interface OrdersMedicineRepository extends JpaRepository<OrdersMedicine, Long>{
	
	List<OrdersMedicine> findByOrdersIdOrderByCreatedDateDesc(Long id);

	List<OrdersMedicine> findByMedicineIdOrderByCreatedDateDesc(Long id);
	
	List<OrdersMedicine> findAllByCreatedDateBetweenOrderByCreatedDateDesc(
		      Date start,
		      Date end);
}
