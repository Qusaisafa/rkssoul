package com.RKSoualApi.repository;


import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.RKSoualApi.model.Medicine;
import com.RKSoualApi.model.Orders;
import com.RKSoualApi.model.OrdersMedicine;


public interface MedicineRepository extends JpaRepository<Medicine, Long>{
	
//	Medicine findByNameAndOrders(String name, Orders order);
	
	Medicine findByName(String name);
	
	List<Medicine> findByCategoryIdOrderByCreatedDateDesc(Long id);
	
	List<Medicine> findAllByOrderByCreatedDateDesc();
	List<Medicine> findAllByCreatedDateBetweenOrderByCreatedDateDesc(
		      Date start,
		      Date end);
}
