package com.RKSoualApi.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.RKSoualApi.model.Medicine;
import com.RKSoualApi.model.MedicineCategory;
import com.RKSoualApi.model.Orders;


public interface MedicineCategoryRepository extends JpaRepository<MedicineCategory, Long>{
	

}
