package com.RKSoualApi.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.RKSoualApi.model.Appointments;


public interface AppointmentRepository extends JpaRepository<Appointments, Long>{
	List<Appointments> findAllByCreatedDateBetweenOrderByCreatedDateDesc(
		      Date start,
		      Date end);
	
	@Query(value = "SELECT COUNT(u) from Appointments u where u.createdDate >= :startDate and u.createdDate < :endDate ")
	public Long getNumberOfAllAppointments(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
}
