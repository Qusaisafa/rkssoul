package com.RKSoualApi.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.RKSoualApi.model.SpendingCategory;


public interface SpendingCategoryRepository extends JpaRepository<SpendingCategory, Long>{
	
}

