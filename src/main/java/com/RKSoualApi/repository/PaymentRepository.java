package com.RKSoualApi.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.RKSoualApi.model.Payments;


public interface PaymentRepository extends JpaRepository<Payments, Long>{
	List<Payments> findByOrdersIdOrderByCreatedDateDesc(Long id);
}
