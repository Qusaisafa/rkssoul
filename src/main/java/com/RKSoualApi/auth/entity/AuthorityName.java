package com.RKSoualApi.auth.entity;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}