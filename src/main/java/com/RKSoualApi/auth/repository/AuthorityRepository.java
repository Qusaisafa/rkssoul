package com.RKSoualApi.auth.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.RKSoualApi.auth.entity.Authority;


public interface AuthorityRepository  extends JpaRepository<Authority , Long> {
}