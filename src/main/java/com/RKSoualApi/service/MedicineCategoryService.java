package com.RKSoualApi.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.RKSoualApi.dto.MedicineCategoryDto;
import com.RKSoualApi.model.MedicineCategory;
import com.RKSoualApi.repository.MedicineCategoryRepository;

@Service
public class MedicineCategoryService {
	
	@Autowired
	private MedicineCategoryRepository medicineCategoryRepository;
	
	@Transactional
	public MedicineCategoryDto addCategory(MedicineCategoryDto CategoryDto) {
		
		MedicineCategory category = new MedicineCategory();
		category.setName(CategoryDto.getName());		
		category= this.medicineCategoryRepository.save(category);
		return new MedicineCategoryDto(category);
	}

	public List<MedicineCategoryDto> getAllCategories() {
		List<MedicineCategory> cats = this.medicineCategoryRepository.findAll();
		List<MedicineCategoryDto> catsdto = new ArrayList<>();
		
		for(MedicineCategory cat : cats) {
			catsdto.add(new MedicineCategoryDto(cat));
		}
		
		return catsdto;
	}

	public MedicineCategoryDto getCatById(Long id) {
		MedicineCategory cat = this.medicineCategoryRepository.findOne(id);
		if(cat == null) {
			throw new RuntimeException("category not found:"+id);
		}
		return new MedicineCategoryDto(cat);
	}

	public MedicineCategoryDto editCatById(Long id, MedicineCategoryDto categoryDto) {
		MedicineCategory cat = this.medicineCategoryRepository.findOne(id);
		if(cat == null) {
			throw new RuntimeException("category not found:"+id);
		}
		cat.setName(categoryDto.getName());		
		cat= this.medicineCategoryRepository.save(cat);
		return new MedicineCategoryDto(cat);
	}
	
	//TODO add delete
}
