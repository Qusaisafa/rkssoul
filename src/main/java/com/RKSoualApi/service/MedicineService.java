package com.RKSoualApi.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.RKSoualApi.dto.MedicineDto;
import com.RKSoualApi.exceptions.MedicineNotFoundException;
import com.RKSoualApi.model.Medicine;
import com.RKSoualApi.model.MedicineCategory;
import com.RKSoualApi.repository.MedicineCategoryRepository;
import com.RKSoualApi.repository.MedicineRepository;

@Service
public class MedicineService {
	
	@Autowired
	private MedicineRepository medicineRepository;
	
	@Autowired
	private MedicineCategoryRepository medicineCategoryRepository;

	@Transactional
	public MedicineDto createMedicine(MedicineDto medicineDto) {
		
		Medicine medicine = new Medicine();
		
		MedicineCategory medicineCategory = this.medicineCategoryRepository.findOne(medicineDto.getCategory().getId());
		
		if(medicineCategory == null) {
			throw new RuntimeException("medicine category not found , add it first");
		}
		
		medicine.setCategory(medicineCategory);
		medicine.setDescription(medicineDto.getDescription());
		medicine.setName(medicineDto.getName());
		medicine.setMedicineStatus(medicineDto.getMedicineStatus());
		medicine.setPrice(medicineDto.getPrice());
		medicine = this.medicineRepository.save(medicine);
		
		return new MedicineDto(medicine);
	}

	public MedicineDto getMedicine(Long id) {
		Medicine med = this.medicineRepository.findOne(id);
		if(med == null) {
			throw new RuntimeException("medicine not found , add it first : "+med);
		}
		return new MedicineDto(med);
	}

	public List<MedicineDto> getAllMedicines() {
		
		List<MedicineDto> medicineDtos = new ArrayList<>();
		for(Medicine medicine :this.medicineRepository.findAllByOrderByCreatedDateDesc()) {
			medicineDtos.add(new MedicineDto(medicine));
		}
		
		return medicineDtos;
	}

	@Transactional
	public MedicineDto editMedicine(long id, MedicineDto medicineDto) {
		
		Medicine medicine = this.medicineRepository.findOne(id);
		if(medicine == null) {
			throw new MedicineNotFoundException();
		}
		
		if(medicineDto.getCategory()!= null) {
			if(medicineDto.getCategory().getId() != null) {
				
				MedicineCategory medicineCategory = this.medicineCategoryRepository.findOne(medicineDto.getCategory().getId());
				
				if(medicineCategory == null) {
					throw new RuntimeException("medicine category not found , add it first");
				}
				medicine.setCategory(medicineCategory);
			}
		}
		
		medicine.setDescription(medicineDto.getDescription());
		medicine.setName(medicineDto.getName());
		medicine.setPrice(medicineDto.getPrice());
		
		medicine = this.medicineRepository.save(medicine);
		return new MedicineDto(medicine);
	}
	
	public List<MedicineDto> findByCategory(Long id){
		
		List<Medicine> mids = this.medicineRepository.findByCategoryIdOrderByCreatedDateDesc(id);

		if(mids == null) {
			throw new RuntimeException("no midecines found for categoryId:"+id);
		}
		List<MedicineDto> midsDto = new ArrayList<>();
		
		for(Medicine mid: mids) {
			midsDto.add(new MedicineDto(mid));
		}
		
		return midsDto;
	}

}
