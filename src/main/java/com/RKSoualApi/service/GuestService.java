package com.RKSoualApi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.RKSoualApi.dto.DatesDto;
import com.RKSoualApi.dto.GuestDto;
import com.RKSoualApi.dto.GuestMoneyReport;
import com.RKSoualApi.dto.GuestSimpileDto;
import com.RKSoualApi.dto.OrderMoneyReportDto;
import com.RKSoualApi.model.Guest;
import com.RKSoualApi.model.Orders;
import com.RKSoualApi.repository.GuestRepository;
import com.RKSoualApi.repository.OrderRepository;

@Service
public class GuestService {

	@Autowired 
	private GuestRepository guestRepository;
	
	@Autowired
	private OrderRepository orderRepository;

	
	@Transactional
	public GuestDto createGuest(GuestDto guestDto) {
		
		Guest guest = new Guest(guestDto);
		guest = this.guestRepository.save(guest);
		
		return new GuestDto(guest);
	}

	public GuestDto getGuest(Long id) {
		Guest guest = this.guestRepository.findOne(id);
		if(guest == null) {
			throw new RuntimeException("guest not found");
		}
		
		return new GuestDto(guest);
	}
	
	public GuestSimpileDto getGuestByName(String name) {
		Guest guest = this.guestRepository.findByName(name);
		if(guest == null) {
			throw new RuntimeException("guest not found:"+name);
		}
		
		return new GuestSimpileDto(guest);
	}
	

	public List<GuestDto> getAllGuests() {
		
		List<Guest> guests = this.guestRepository.findAllByOrderByCreatedDateDesc();
		List<GuestDto> guestsDto = new ArrayList<>();
		for(Guest guest:guests) {
			guestsDto.add(new GuestDto(guest));
		}
		return guestsDto;
	}
	
	public List<GuestMoneyReport> getAllGuestsWithMonyStill() {
		
		List<Orders> orders = this.orderRepository.getOrdersWithMonyStill();
		
		Map<Long, List<Orders>> map = new HashMap<Long, List<Orders>>();
		for(Orders entry : orders) {
			Long id = entry.getGuest().getId();
			if(map.containsKey(id)) {
				map.get(id).add(entry);
			}else {
				List<Orders> ooo = new ArrayList<Orders>();
				ooo.add(entry);
				map.put(id, ooo);
			}
			
		}
		List<GuestMoneyReport> guestsReports = new ArrayList<>();
		for (Map.Entry<Long, List<Orders>> entry2 : map.entrySet()) {
			GuestMoneyReport guestMoneyReport = new GuestMoneyReport();
			guestMoneyReport.setGuestId(entry2.getKey());
			guestMoneyReport.setGuestName(entry2.getValue().get(0).getGuest().getName());
			float totalMoneyStill = 0;
			float totalMoneyPaid = 0;
			float totalPrice = 0;
			for(Orders order2: entry2.getValue()) {
				totalMoneyStill += order2.getMoneyStill();
				totalMoneyPaid += order2.getMoneyPaid();
				totalPrice += order2.getTotalPrice();
				guestMoneyReport.getOrders().add(new OrderMoneyReportDto(order2));
			}
			
			guestMoneyReport.setTotalMoneyPaid(totalMoneyPaid);
			guestMoneyReport.setTotalMoneyStill(totalMoneyStill);
			guestMoneyReport.setTotalprice(totalPrice);
			guestsReports.add(guestMoneyReport);
		}

		return guestsReports;
	}
	
	public List<GuestDto> getAllGuestsByPages(int number, int size) {
		Pageable pageOf = new PageRequest(number, size);
		Page<Guest> guests = this.guestRepository.findAll(pageOf);
		List<GuestDto> guestsDto = new ArrayList<>();
		for(Guest guest:guests.getContent()) {
			guestsDto.add(new GuestDto(guest));
		}
		return guestsDto;
	}
	@Transactional
	public GuestDto editGuest(long id, GuestDto guestDto) {
		Guest guest = this.guestRepository.findOne(id);
		if(guest == null) {
			throw new RuntimeException("guest not found");
		}
		guest.setAddress(guestDto.getAddress());
		guest.setName(guestDto.getName());
		guest.setPhone(guestDto.getPhone());
		guest.setWeight(guestDto.getWeight());
		guest.setAge(guestDto.getAge());
		guest = this.guestRepository.save(guest);
		return new GuestDto(guest);
	}
	
	public GuestMoneyReport getGuestMoneyReport(Long id) {
		List<Orders> orders = this.orderRepository.findByGuestIdOrderByCreatedDateDesc(id);
		if(orders == null) {
			throw new RuntimeException("no orders for this guestId:"+id);
		}
		
		GuestMoneyReport report = new GuestMoneyReport();
		
		float totalMoneyStill = 0;
		
		float totalMoneyPaid = 0;
		
		float totalprice = 0;
		for(Orders order :orders) {
			totalMoneyStill += order.getMoneyStill();
			totalMoneyPaid += order.getMoneyPaid();
			totalprice += order.getTotalPrice();
			report.getOrders().add(new OrderMoneyReportDto(order));
		}
		report.setTotalMoneyPaid(totalMoneyPaid);
		report.setTotalMoneyStill(totalMoneyStill);
		report.setTotalprice(totalprice);
		
		return report;
	}

	public Long getNumberOfGuestssFromTo(DatesDto dates) {
		 
		return this.guestRepository.getNumberOfAllGuests(dates.getStart(), dates.getEnd());
	}


}
