package com.RKSoualApi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.RKSoualApi.dto.AppointmentsDto;
import com.RKSoualApi.dto.DatesDto;
import com.RKSoualApi.dto.MedicineNumbersByMonths;
import com.RKSoualApi.dto.MedicineSessionsReport;
import com.RKSoualApi.dto.OrderDto;
import com.RKSoualApi.dto.OrdersAppointmentsDto;
import com.RKSoualApi.dto.PaymentDto;
import com.RKSoualApi.dto.OrderMedicineDto;
import com.RKSoualApi.exceptions.OrderNotFoundException;
import com.RKSoualApi.model.Appointments;
import com.RKSoualApi.model.Guest;
import com.RKSoualApi.model.Medicine;
import com.RKSoualApi.model.Orders;
import com.RKSoualApi.model.OrdersMedicine;
import com.RKSoualApi.model.Payments;
import com.RKSoualApi.repository.AppointmentRepository;
import com.RKSoualApi.repository.GuestRepository;
import com.RKSoualApi.repository.MedicineRepository;
import com.RKSoualApi.repository.OrderRepository;
import com.RKSoualApi.repository.OrdersMedicineRepository;
import com.RKSoualApi.repository.PaymentRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private MedicineRepository medicineRepository;
	
	@Autowired
	private AppointmentRepository appointmentsRepository;

	@Autowired
	private GuestRepository guestRespository;
	
	@Autowired
	private OrdersMedicineRepository ordersMedicineRespository;
	
	
	@Autowired
	private PaymentRepository paymentRepository;
	
	
	
	@Transactional
	public OrderDto createOrder(OrderDto orderDto) {

		Orders order = new Orders();

		Guest guest = guestRespository.findOne(orderDto.getGuest().getId());
		if (guest == null) {
			throw new RuntimeException("guest not found:"+orderDto.getGuest().getId());
		}
		
		for (AppointmentsDto app :orderDto.getAppointments()) {
			Appointments appnew = new Appointments(app);
			appnew.setOrders(order);
			order.getAppointments().add(appnew);
		}
		
		order.setGuest(guest);
		order.setMedicineStatus(orderDto.getMedicineStatus());
		order.setDescription(orderDto.getDescription());
		guest.getOrders().add(order);

		float totalPrice = 0;
		for (OrderMedicineDto medicine : orderDto.getMedicines()) {
			Medicine savedMedicine = medicineRepository.findOne(medicine.getMedicineId());
			if (savedMedicine == null) {
				throw new RuntimeException("midicine not found, ID:"+medicine.getMedicineId());
			}
			OrdersMedicine orderMedicineJoinTable = new OrdersMedicine();
			orderMedicineJoinTable.setMedicine(savedMedicine);
			orderMedicineJoinTable.setOrders(order);
			orderMedicineJoinTable.setNumberOfSessions(medicine.getNumberOfSessions());
			float userPrice = medicine.getUserPrice();
			if(userPrice == 0) {
				userPrice = savedMedicine.getPrice();
			}
			totalPrice += userPrice;
			orderMedicineJoinTable.setUserPrice(userPrice);
			savedMedicine.getOrdersMedicine().add(orderMedicineJoinTable);
			order.getOrdersMedicine().add(orderMedicineJoinTable);
			this.ordersMedicineRespository.save(orderMedicineJoinTable);
			
		}
		
		float moneyPaid = orderDto.getMoneyPaid();
		order.setTotalPrice(totalPrice);
		order.setMoneyPaid(moneyPaid);
		order.setMoneyStill(totalPrice - moneyPaid);
		order.setMoneyStatus(orderDto.getMoneyStatus());

		order = this.orderRepository.save(order);
		
		return new OrderDto(order);
	}

	public OrderDto getOrder(Long id) {
		Orders order = this.orderRepository.findOne(id);
		return new OrderDto(order);

	}

	public List<OrderDto> getAllOrders() {
		List<Orders> orders = this.orderRepository.findAllByOrderByCreatedDateDesc();
		List<OrderDto> results = new ArrayList<>();
		for (Orders order : orders) {
			results.add(new OrderDto(order));
		}

		return results;

	}

	@Transactional
	public OrderDto editOrder(long id, OrderDto orderDto) {
		Orders order = this.orderRepository.findOne(id);
		if (order == null) {
			throw new OrderNotFoundException();
		}

		Guest guest = guestRespository.findOne(orderDto.getGuest().getId());
		if (guest == null) {
			throw new RuntimeException("guest not found:"+orderDto.getGuest().getId());
		}
		
		for (AppointmentsDto app :orderDto.getAppointments()) {
			Appointments appnew = new Appointments(app);
			appnew.setOrders(order);
			order.getAppointments().add(appnew);
		}
		
		order.setGuest(guest);
		order.setMedicineStatus(orderDto.getMedicineStatus());
		order.setDescription(orderDto.getDescription());
		guest.getOrders().add(order);

		float totalPrice = 0;
		for (OrderMedicineDto medicine : orderDto.getMedicines()) {
			Medicine savedMedicine = medicineRepository.findOne(medicine.getMedicineId());
			if (savedMedicine == null) {
				throw new RuntimeException("midicine not found, ID:"+medicine.getMedicineId());
			}
			OrdersMedicine orderMedicineJoinTable = new OrdersMedicine();
			orderMedicineJoinTable.setMedicine(savedMedicine);
			orderMedicineJoinTable.setOrders(order);
			float userPrice = medicine.getUserPrice();
			orderMedicineJoinTable.setNumberOfSessions(medicine.getNumberOfSessions());
			if(userPrice == 0) {
				userPrice = savedMedicine.getPrice();
			}
			totalPrice += userPrice;
			orderMedicineJoinTable.setUserPrice(userPrice);
			savedMedicine.getOrdersMedicine().add(orderMedicineJoinTable);
			order.getOrdersMedicine().add(orderMedicineJoinTable);
			this.ordersMedicineRespository.save(orderMedicineJoinTable);
			
		}
		
		float moneyPaid = orderDto.getMoneyPaid();
		order.setTotalPrice(totalPrice);
		order.setMoneyPaid(moneyPaid);
		order.setMoneyStill(totalPrice - moneyPaid);
		order.setMoneyStatus(orderDto.getMoneyStatus());

		order = this.orderRepository.save(order);
		
		return new OrderDto(order);
	}
	
	

	public List<OrderDto> getOrdersByGuest(Long id){
		
		List<Orders> orders = this.orderRepository.findByGuestIdOrderByCreatedDateDesc(id);
		List<OrderDto> results = new ArrayList<>();
		for (Orders order : orders) {
			results.add(new OrderDto(order));
		}

		return results;

	}
	
	public List<OrderDto> getAllOrdersByPages(int pageNumber, int size ){
		Pageable pageOf = new PageRequest(pageNumber, size);
		Page<Orders> orders = this.orderRepository.findAll(pageOf);
		
		List<OrderDto> results = new ArrayList<>();
		for (Orders order : orders.getContent()) {
			results.add(new OrderDto(order));
		}

		return results;

	}
	
	@Transactional
	public OrderDto pay(PaymentDto paymentDto) {
		Orders order = this.orderRepository.findOne(paymentDto.getOrderId());
		if (order == null) {
			throw new OrderNotFoundException();
		}
		
		Payments payment = new Payments();
		payment.setMoney(paymentDto.getMoneyAmount());
		payment.setOrders(order);
		payment.setCreatedDate(paymentDto.getDate());
		payment.setType(paymentDto.getType());
		payment.setStatus(paymentDto.getStatus());
		order.getPayments().add(payment);
		
		float moneyPaid = order.getMoneyPaid() + paymentDto.getMoneyAmount();
		float totalPrice = order.getTotalPrice();
		order.setMoneyPaid(moneyPaid);
		order.setMoneyStill(totalPrice - moneyPaid);
		order = this.orderRepository.save(order);
		
		return new OrderDto(order);
	}
	
	@Transactional
	public OrdersAppointmentsDto addAppointments(Long id, OrdersAppointmentsDto ordersAppointments) {
		Orders order = this.orderRepository.findOne(id);
		if (order == null) {
			throw new OrderNotFoundException();
		}

		for (AppointmentsDto app: ordersAppointments.getAppointments()) {
			Appointments appnew = new Appointments(app);
			appnew.setOrders(order);
			order.getAppointments().add(appnew);
			Float price = app.getPrice();
			if(price != null) {
				float orderTotalPrice = order.getTotalPrice();	
				order.setTotalPrice(orderTotalPrice + price);
			}
		}
		
		order = this.orderRepository.save(order);

		Set<AppointmentsDto> appointmentsSet = new HashSet<>();
		
		for(Appointments appointment: order.getAppointments()) {
			appointmentsSet.add(new AppointmentsDto(appointment));
		}
		OrdersAppointmentsDto orderAppointments = new OrdersAppointmentsDto();
		orderAppointments.setAppointments(appointmentsSet);
		
		return orderAppointments;
	}
	
	public Long getNumberOfReportsFromTo(DatesDto dates) {
		return this.orderRepository.getNumberOfAllOrders(dates.getStart(), dates.getEnd());
	}
	
	public List<PaymentDto>getMoneyReportForOrder(Long id){
		Orders order = this.orderRepository.findOne(id);
		if (order == null) {
			throw new OrderNotFoundException(id);
		}
		List<Payments> payments = this.paymentRepository.findByOrdersIdOrderByCreatedDateDesc(id);
		List<PaymentDto> paymentsDto = new ArrayList<>();
		for(Payments payment:payments) {
			paymentsDto.add(new PaymentDto(payment));
		}
		
		return paymentsDto;
	}
	public Set<AppointmentsDto> getAppointments(Long id){
		Orders order = this.orderRepository.findOne(id);
		if (order == null) {
			throw new OrderNotFoundException(id);
		}
		
		Set<AppointmentsDto> appsDto = new HashSet<>();
		for(Appointments app : order.getAppointments()) {
			appsDto.add(new AppointmentsDto(app));
		}
		return appsDto;
		
	}

	public Long SumOfMoneyStill(DatesDto dates) {
		return this.orderRepository.getSumOfMoneyStill(dates.getStart(), dates.getEnd());
	}
	
	public Long sumOfMoneyPaid(DatesDto dates) {
		return this.orderRepository.getSumOfMoneyPaid(dates.getStart(), dates.getEnd());
	}
	
	public List<MedicineSessionsReport> getMedicineSessionsReport(DatesDto dates) {
		List<OrdersMedicine> medicinesSessions = this.ordersMedicineRespository.findAllByCreatedDateBetweenOrderByCreatedDateDesc(dates.getStart(), dates.getEnd());
		List<Medicine> medicines = this.medicineRepository.findAll();
		
		Map<Long, Medicine> medicinesMap = new HashMap<Long, Medicine>();
		for(Medicine e : medicines) {
			medicinesMap.put(e.getId(), e);
		}
		
		Map<Long, Integer> map = new HashMap<Long, Integer>();
		
		for(OrdersMedicine e : medicinesSessions) {
			Long id = e.getMedicine().getId();
			int count = map.containsKey(id) ? map.get(id) : 0;
			map.put(id, count + 1);
		}
		
		List<MedicineSessionsReport> medicineSessionsReport = new ArrayList<>();
		 for (Entry<Long, Integer> entry : map.entrySet()) {
			 Medicine medicine = medicinesMap.get(entry.getKey());
			 medicineSessionsReport.add(new MedicineSessionsReport(entry.getKey(), medicine.getName(), entry.getValue()));
		 }
		return medicineSessionsReport;
	}
	

	public List<MedicineNumbersByMonths> getMedicineSessionsReportByMonth(DatesDto dates) {
		List<OrdersMedicine> medicinesSessions = this.ordersMedicineRespository.findAllByCreatedDateBetweenOrderByCreatedDateDesc(dates.getStart(), dates.getEnd());
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		for(OrdersMedicine e : medicinesSessions) {
			int month = e.getCreatedDate().getMonth();
			int count = map.containsKey(month) ? map.get(month) : 0;
			map.put(month, count + 1);
		}
		
		List<MedicineNumbersByMonths> report = new ArrayList<>();
		 for (Entry<Integer, Integer> entry : map.entrySet()) {
			 report.add(new MedicineNumbersByMonths(entry.getKey(), entry.getValue()));
		 }
		return report;
	}
}
