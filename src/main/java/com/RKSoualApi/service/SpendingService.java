package com.RKSoualApi.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.RKSoualApi.dto.DatesDto;
import com.RKSoualApi.dto.MedicineCategoryDto;
import com.RKSoualApi.dto.MedicineSessionsReport;
import com.RKSoualApi.dto.SpendingCategoryDto;
import com.RKSoualApi.dto.SpendingCategoryReport;
import com.RKSoualApi.dto.SpendingDto;
import com.RKSoualApi.dto.SpendingReport;
import com.RKSoualApi.model.Medicine;
import com.RKSoualApi.model.MedicineCategory;
import com.RKSoualApi.model.OrdersMedicine;
import com.RKSoualApi.model.Spending;
import com.RKSoualApi.model.SpendingCategory;
import com.RKSoualApi.repository.SpendingCategoryRepository;
import com.RKSoualApi.repository.SpendingRepository;

@Service
public class SpendingService {
	
	@Autowired 
	private SpendingRepository spendingRepository;
	
	@Autowired 
	private SpendingCategoryRepository spendingCategoryRepository;


	@Transactional
	public SpendingDto addSpending(SpendingDto spendingDto) {
		
		Spending newSpending = new Spending(spendingDto);
		SpendingCategory category = this.spendingCategoryRepository.findOne(spendingDto.getCategory().getId());
		if(category == null) {
			throw new RuntimeException("category not found :"+spendingDto.getCategory().getId());
		}
		
		newSpending.setCategory(category);
		newSpending = this.spendingRepository.save(newSpending);
		
		return new SpendingDto(newSpending);
	}

	public SpendingDto getSpending(Long id) {
		Spending spending = this.spendingRepository.findOne(id);
		if(spending == null) {
			throw new RuntimeException("spending not found :"+id);
		}
		return new SpendingDto(spending);
		
	}

	public List<SpendingDto> getAllSpendingBetweenDates(DatesDto dates) {
		
		List<Spending>  spendings = this.spendingRepository.findAllByCreatedDateBetweenOrderByCreatedDateDesc(dates.getStart(), dates.getEnd());
		List<SpendingDto> spendingsDto = new ArrayList<>();
		for(Spending e:spendings) {
			spendingsDto.add(new SpendingDto(e));
		}
		return spendingsDto;
	}

	public SpendingReport getSpendingReportForCategory(String category) {
		List<Spending>  spendings = this.spendingRepository.findByCategoryNameOrderByCreatedDateDesc(category);
		List<SpendingDto> spendingsDto = new ArrayList<>();
		for(Spending e:spendings) {
			spendingsDto.add(new SpendingDto(e));
		}
		return new SpendingReport(spendingsDto);
	}
	@Transactional
	public SpendingDto editSpending(Long id, SpendingDto spendingDto) {
		Spending spending = this.spendingRepository.findOne(id);
		if(spending == null) {
			throw new RuntimeException("spending not found :"+id);
		}
		
		SpendingCategory category = this.spendingCategoryRepository.findOne(spendingDto.getCategory().getId());
		if(category == null) {
			throw new RuntimeException("category not found :"+spendingDto.getCategory().getId());
		}
		
		
		spending.setAmount(spendingDto.getAmount());
		spending.setCategory(category);
		spending.setDescription(spendingDto.getDescription());
		spending.setName(spendingDto.getName());
		spending= this.spendingRepository.save(spending);
		return new SpendingDto(spending);
	}
	
	
	@Transactional
	public SpendingCategoryDto editCat(Long id, SpendingCategoryDto spendingDto) {
		
		SpendingCategory category = this.spendingCategoryRepository.findOne(id);
		if(category == null) {
			throw new RuntimeException("category not found :"+id);
		}
		category.setName(spendingDto.getName());
		category = this.spendingCategoryRepository.save(category);
		return new SpendingCategoryDto(category);
	}
	
	public List<SpendingCategoryReport> getSpendingsReports(DatesDto dates) {
		List<Spending> spendings = this.spendingRepository.findAllByCreatedDateBetweenOrderByCreatedDateDesc(dates.getStart(), dates.getEnd());
		List<SpendingCategory> categories = this.spendingCategoryRepository.findAll();
		
		Map<Long, SpendingCategory> categoryMap = new HashMap<Long, SpendingCategory>();
		for(SpendingCategory e : categories) {
			categoryMap.put(e.getId(), e);
		}
		
		Map<Long, Float> map = new HashMap<Long, Float>();
		
		for(Spending e : spendings) {
			float sum = map.containsKey(e.getId()) ? map.get(e.getId()) : 0;
			map.put(e.getCategory().getId(), sum + e.getAmount());
		}
		
		List<SpendingCategoryReport> spendingCategoryReport = new ArrayList<>();
		 for (Entry<Long, Float> entry : map.entrySet()) {
			 SpendingCategory cat = categoryMap.get(entry.getKey());
			 SpendingCategoryReport ss =  new SpendingCategoryReport(entry.getKey(), cat.getName(), entry.getValue());
			 spendingCategoryReport.add(ss);
		 }
		return spendingCategoryReport;
	}

	public SpendingCategoryDto createCategory(SpendingCategoryDto spendingCategoryDto) {
		
		SpendingCategory spendingCategory = new SpendingCategory();
		spendingCategory.setName(spendingCategoryDto.getName());
		spendingCategory = this.spendingCategoryRepository.save(spendingCategory);
		
		return new SpendingCategoryDto(spendingCategory);
	}

	public List<SpendingDto> getAllSpendingsByPages(int number, int size) {
		Pageable pageOf = new PageRequest(number, size);
		Page<Spending>  spendings = this.spendingRepository.findAll(pageOf);
		List<SpendingDto> spendingsDto = new ArrayList<>();
		for(Spending e:spendings.getContent()) {
			spendingsDto.add(new SpendingDto(e));
		}
		return spendingsDto;
	}

	public List<SpendingCategoryDto> getAllCategories() {
		List<SpendingCategory> cats = this.spendingCategoryRepository.findAll();
		List<SpendingCategoryDto> catsdto = new ArrayList<>();
		
		for(SpendingCategory cat : cats) {
			catsdto.add(new SpendingCategoryDto(cat));
		}
		
		return catsdto;
	}

	public SpendingCategoryDto getCatById(Long id) {
		SpendingCategory category = this.spendingCategoryRepository.findOne(id);
		if(category == null) {
			throw new RuntimeException("category not found :"+id);
		}
		return new SpendingCategoryDto(category);
	}
	
	
	public Double sumOfTotalMoneySpend(DatesDto dates) {
		Double sum = this.spendingRepository.getTotalSpendings(dates.getStart(), dates.getEnd());
		if(sum == null) {
			return 0.0;
		}
		return sum;
	}
}
