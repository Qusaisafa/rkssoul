package com.RKSoualApi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.RKSoualApi.dto.AppointmentsDto;
import com.RKSoualApi.dto.OrdersAppointmentsDto;
import com.RKSoualApi.model.Appointments;
import com.RKSoualApi.model.Orders;
import com.RKSoualApi.repository.AppointmentRepository;
import com.RKSoualApi.repository.OrderRepository;

@Service
public class AppointmentService {

	@Autowired
	private AppointmentRepository appointmentRepository;
	
	@Autowired
	private OrderRepository orderRepository;

	
	
	public List<AppointmentsDto> getAppointmentsForOrder(Long id) {
		
		Orders order = this.orderRepository.findOne(id);
		if(order == null) {
			throw new RuntimeException("order not found"); 
		}
		
		List<Appointments> appointments = order.getAppointments();
		List<AppointmentsDto> appSet = new ArrayList<>();
		
		for(Appointments app : appointments) {
			appSet.add(new AppointmentsDto(app));
		}
		
		return appSet;
	}
	
	@Transactional
	public AppointmentsDto editAppointment(Long id, AppointmentsDto appDto) {
		Appointments app = this.appointmentRepository.findOne(id);
		if(app == null) {
			throw new RuntimeException("appointment not found");
		}
		
		app.setDate(appDto.getDate());
		app.setNote(appDto.getNote());
		app.setStatus(appDto.getStatus());
		app.setMedicineName(appDto.getMedicineName());
		Float price = appDto.getPrice();
		float totalPrice = app.getOrders().getTotalPrice();
		if(price != null && price > 0) {
			totalPrice = totalPrice - price;
		}
		Float newPrice = appDto.getPrice();
		app.setPrice(newPrice);
		app.getOrders().setTotalPrice(totalPrice + newPrice);
		this.appointmentRepository.save(app);
		return new AppointmentsDto(app);
	}
	
	public List<AppointmentsDto> getAllAppointmentsBetweenDates(Date start, Date end) {
		
		List<Appointments> appointments = this.appointmentRepository.findAllByCreatedDateBetweenOrderByCreatedDateDesc(start, end);
		if(appointments == null) {
			throw new RuntimeException("appointments not found"); 
		}
		
		List<AppointmentsDto> appSet = new ArrayList<>();
		
		for(Appointments app : appointments) {
			appSet.add(new AppointmentsDto(app));
		}
		
		return appSet;
	}
	
	public Long getNumberAppointmentsBetweenDates(Date start, Date end) {
		
		return this.appointmentRepository.getNumberOfAllAppointments(start, end);
		
	}

	
}
