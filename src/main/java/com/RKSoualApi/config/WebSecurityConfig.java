package com.RKSoualApi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.RKSoualApi.auth.entity.AuthorityName;
import com.RKSoualApi.auth.security.JwtAuthenticationEntryPoint;
import com.RKSoualApi.auth.security.JwtAuthenticationTokenFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtAuthenticationEntryPoint unauthorizedHandler;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
        return new JwtAuthenticationTokenFilter();
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web.debug(true);
    }
    
 
    
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
    	httpSecurity
    	.csrf().disable()
	    	.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
	    	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
	    	.authorizeRequests()
    		.antMatchers(HttpMethod.OPTIONS).permitAll()
	    	.antMatchers(HttpMethod.GET,"/swagger-ui**").permitAll()
	    	.antMatchers(HttpMethod.GET, "/api/user/disable/**").hasAuthority(AuthorityName.ROLE_ADMIN.name())
	    	.antMatchers(HttpMethod.POST, "/api/order/ordersBetween/**").hasAuthority(AuthorityName.ROLE_ADMIN.name())
	    	.antMatchers(HttpMethod.GET, "/api/guest/reportMoney/**").hasAuthority(AuthorityName.ROLE_ADMIN.name())
	    	.antMatchers(HttpMethod.POST, "/api/guest/guestsBetween/**").hasAuthority(AuthorityName.ROLE_ADMIN.name())
	    	.antMatchers("/api/**").authenticated()
	    	.anyRequest().permitAll();


        httpSecurity
                .addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);

        httpSecurity.headers().cacheControl();
    }
}