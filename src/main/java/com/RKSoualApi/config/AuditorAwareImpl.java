package com.RKSoualApi.config;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import com.RKSoualApi.auth.entity.User;
import com.RKSoualApi.auth.service.UserService;

public class AuditorAwareImpl implements AuditorAware<User> {

	@Autowired
	private UserService userService;

	@Override
	public User getCurrentAuditor() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		return userService.findUserByUserName(username);
		
	}
}
