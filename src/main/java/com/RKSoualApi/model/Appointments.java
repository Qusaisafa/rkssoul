package com.RKSoualApi.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


import com.RKSoualApi.dto.AppointmentsDto;

@Entity

public class Appointments extends Audit{

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String MedicineName;
	
	private Long medicineId;

	private Date date;
	
	private String status;
	
	private String note;
	
	private Float price;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orders_id", nullable = false)
	private Orders orders;


	public Appointments(){
		
	}

	public Appointments(AppointmentsDto app){
		this.MedicineName = app.getMedicineName();
		this.status = app.getStatus();
		this.medicineId = app.getMedicineId();
		this.date = app.getDate();
		this.note = app.getNote();
		this.price = app.getPrice();
	}

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getMedicineName() {
		return MedicineName;
	}


	public void setMedicineName(String medicineName) {
		MedicineName = medicineName;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public Orders getOrders() {
		return orders;
	}


	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getMedicineId() {
		return medicineId;
	}

	public void setMedicineId(Long medicineId) {
		this.medicineId = medicineId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	
	
}
