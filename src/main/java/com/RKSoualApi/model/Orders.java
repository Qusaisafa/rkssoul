package com.RKSoualApi.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;




@Entity
public class Orders extends Audit{
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@OneToMany(mappedBy = "orders")
    private List<OrdersMedicine> ordersMedicine = new ArrayList<OrdersMedicine>();
    
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "guest_id", nullable = false)
	private Guest guest;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "orders")
	private List<Appointments> appointments = new ArrayList<>();
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "orders")
	private List<Payments> payments = new ArrayList<>();

	private String medicineStatus;
	
	private String moneyStatus;
	
	private String description;
	
	private float totalPrice;
	
	private float moneyPaid;
	
	private float moneyStill;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getMedicineStatus() {
		return medicineStatus;
	}

	public void setMedicineStatus(String medicineStatus) {
		this.medicineStatus = medicineStatus;
	}

	public String getMoneyStatus() {
		return moneyStatus;
	}

	public void setMoneyStatus(String moneyStatus) {
		this.moneyStatus = moneyStatus;
	}

	public float getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}

	public float getMoneyPaid() {
		return moneyPaid;
	}

	public void setMoneyPaid(float moneyPaid) {
		this.moneyPaid = moneyPaid;
	}

	public float getMoneyStill() {
		return moneyStill;
	}

	public void setMoneyStill(float moneyStill) {
		this.moneyStill = moneyStill;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}

	public List<OrdersMedicine> getOrdersMedicine() {
		return ordersMedicine;
	}

	public void setOrdersMedicine(List<OrdersMedicine> ordersMedicine) {
		this.ordersMedicine = ordersMedicine;
	}

	public List<Appointments> getAppointments() {
		return appointments;
	}

	public void setAppointments(List<Appointments> appointments) {
		this.appointments = appointments;
	}

	public List<Payments> getPayments() {
		return payments;
	}

	public void setPayments(List<Payments> payments) {
		this.payments = payments;
	}
	
	
}
