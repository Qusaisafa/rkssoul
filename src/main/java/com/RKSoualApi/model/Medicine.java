
package com.RKSoualApi.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.RKSoualApi.dto.MedicineDto;


@Entity
public class Medicine extends Audit{

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String name;
	
	private String description;
	
	private String medicineStatus;
	
	@Column(name="price", nullable = false)
	private float price;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", nullable = false)
	private MedicineCategory category;
	
    @OneToMany(mappedBy = "medicine")
    private Set<OrdersMedicine> ordersMedicine = new HashSet<OrdersMedicine>();
	
	public Medicine() {
		
	}
	
	public Medicine(MedicineDto medicine) {
		this.name = medicine.getName();
		this.description = medicine.getDescription();
		this.medicineStatus = medicine.getMedicineStatus();
	}

	
	public Set<OrdersMedicine> getOrdersMedicine() {
		return ordersMedicine;
	}

	public void setOrdersMedicine(Set<OrdersMedicine> ordersMedicine) {
		this.ordersMedicine = ordersMedicine;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMedicineStatus() {
		return medicineStatus;
	}

	public void setMedicineStatus(String medicineStatus) {
		this.medicineStatus = medicineStatus;
	}


	public void setCategory(MedicineCategory category) {
		this.category = category;
	}

	public MedicineCategory getCategory() {
		return category;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
}
