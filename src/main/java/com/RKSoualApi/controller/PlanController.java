package com.RKSoualApi.controller;
//package com.translatorApi.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//import com.translatorApi.dto.OrderDto;
//import com.translatorApi.service.OrderService;
//
//public class PlanController {
//
//	@RequestMapping(value="/api/plan")
//	@Controller
//	public class OrderController {
//
//		@Autowired
//		private OrderService orderService;
//		
//		@PostMapping
//		public ResponseEntity<OrderDto> createOrder(@RequestBody OrderDto orderDto){
//			return ResponseEntity.ok().body(this.orderService.createOrder(orderDto));
//		}
//		
//		@GetMapping("/{id}")
//		public ResponseEntity<OrderDto> getOrder(@PathVariable("id") Long id){
//			return ResponseEntity.ok().body(this.orderService.getOrder(id));
//		}
//		
//		@GetMapping
//		public ResponseEntity<List<OrderDto>> getAllOrders(){
//			return ResponseEntity.ok().body(this.orderService.getAllOrders());
//		}
//		
//		@PutMapping("/{id}")
//		public ResponseEntity<OrderDto> editorder(@PathVariable("id") Long id,@RequestBody OrderDto orderDto){
//			return ResponseEntity.ok().body(this.orderService.editOrder(id, orderDto));
//		}
//
//}
