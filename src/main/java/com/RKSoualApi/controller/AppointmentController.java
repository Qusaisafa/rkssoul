package com.RKSoualApi.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.RKSoualApi.dto.AppointmentsDto;
import com.RKSoualApi.dto.DatesDto;
import com.RKSoualApi.dto.OrdersAppointmentsDto;
import com.RKSoualApi.service.AppointmentService;
import com.RKSoualApi.service.OrderService;


@RequestMapping(value="/api/appointment")
@Controller
public class AppointmentController {

	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private AppointmentService appointmentService;
	
	@PostMapping("/order/{id}")
	public ResponseEntity<OrdersAppointmentsDto> createAppointment(@PathVariable("id") long id, @RequestBody OrdersAppointmentsDto appointments){
		return ResponseEntity.ok().body(this.orderService.addAppointments(id, appointments));
	}
	
	@GetMapping("/order/{id}")
	public ResponseEntity<Set<AppointmentsDto>> getAppointmentsForOrder(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.orderService.getAppointments(id));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<AppointmentsDto> editAppointment(@PathVariable("id") Long id, @RequestBody AppointmentsDto appointmentDto){
		return ResponseEntity.ok().body(this.appointmentService.editAppointment(id, appointmentDto));
	}
	
	@PostMapping("/appointments/betweenDates")
	public ResponseEntity<List<AppointmentsDto>> getBetweenDates(@RequestBody DatesDto dates){
		return ResponseEntity.ok().body(this.appointmentService.getAllAppointmentsBetweenDates(dates.getStart(), dates.getEnd()));
	}
	
	@PostMapping("/report/number/betweenDates")
	public ResponseEntity<Long> getNumberOfAppBetweenDates(@RequestBody DatesDto dates){
		return ResponseEntity.ok().body(this.appointmentService.getNumberAppointmentsBetweenDates(dates.getStart(), dates.getEnd()));
	}
}
