package com.RKSoualApi.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.RKSoualApi.dto.DatesDto;
import com.RKSoualApi.dto.GuestDto;
import com.RKSoualApi.dto.GuestMoneyReport;
import com.RKSoualApi.dto.MedicineDto;
import com.RKSoualApi.service.GuestService;
import com.RKSoualApi.service.MedicineService;

@RequestMapping(value="/api/guest")
@Controller
public class GuestController {
	

	@Autowired 
	private GuestService guestService;
	
	@PostMapping
	public ResponseEntity<GuestDto> createGuest(@RequestBody GuestDto guestDto){
		return ResponseEntity.ok().body(this.guestService.createGuest(guestDto));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> getGuests(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.guestService.getGuest(id));
	}
	
	@GetMapping("/filter/{name}")
	public ResponseEntity<Object> getGuestsByName(@PathVariable("name") String name){
		return ResponseEntity.ok().body(this.guestService.getGuestByName(name));
	}
	
	
	@GetMapping
	public ResponseEntity<List<GuestDto>> getAllGuests(){
		return ResponseEntity.ok().body(this.guestService.getAllGuests());
		
	}
	
	@GetMapping("/report/moneyStill")
	public ResponseEntity<List<GuestMoneyReport>> getAllGuestsWithMonystill(){
		return ResponseEntity.ok().body(this.guestService.getAllGuestsWithMonyStill());
		
	}
	
	@GetMapping("/v2/{number}/{size}")
	public ResponseEntity<List<GuestDto>> getAllGuestsByPages(@PathVariable("number") int number, @PathVariable("size") int size){
		return ResponseEntity.ok().body(this.guestService.getAllGuestsByPages(number, size));
		
	}
	
	@GetMapping("/report/money/{id}")
	public ResponseEntity<GuestMoneyReport> getMoneyReportForGuest(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.guestService.getGuestMoneyReport(id));
	}
		
	@PutMapping("/{id}")
	public ResponseEntity<GuestDto> editGuest(@PathVariable("id") long id,@RequestBody GuestDto guestDto){
		return ResponseEntity.ok().body(this.guestService.editGuest(id, guestDto));
	}
	
	
	@PostMapping("/guestsBetween")
	public ResponseEntity<Long> getNumberOfGuestsBetweenDates(@RequestBody DatesDto dates){
		return ResponseEntity.ok().body(this.guestService.getNumberOfGuestssFromTo(dates));
		
	}

}
