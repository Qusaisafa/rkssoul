package com.RKSoualApi.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.RKSoualApi.dto.OrderDto;
import com.RKSoualApi.dto.PaymentDto;
import com.RKSoualApi.service.OrderService;

@RequestMapping(value="/api/payment")
@Controller
public class PaymentController {

	
	@Autowired
	private OrderService orderService ;

	@PostMapping
	public ResponseEntity<OrderDto> payforOrder(@RequestBody PaymentDto paymentDto){
		return ResponseEntity.ok().body(this.orderService.pay(paymentDto));
	}
	
	@GetMapping("/report/order/{id}")
	public ResponseEntity<List<PaymentDto>> getMoneyReportForOrder(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.orderService.getMoneyReportForOrder(id));
	}
}
