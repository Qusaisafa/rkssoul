package com.RKSoualApi.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.RKSoualApi.dto.DatesDto;
import com.RKSoualApi.dto.MedicineNumbersByMonths;
import com.RKSoualApi.dto.MedicineSessionsReport;
import com.RKSoualApi.dto.OrderDto;
import com.RKSoualApi.service.OrderService;

@RequestMapping(value="/api/order")
@Controller
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	@PostMapping
	public ResponseEntity<OrderDto> createOrder(@RequestBody OrderDto orderDto){
		return ResponseEntity.ok().body(this.orderService.createOrder(orderDto));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<OrderDto> getOrder(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.orderService.getOrder(id));
	}
	
	@GetMapping
	public ResponseEntity<List<OrderDto>> getAllOrders(){
		return ResponseEntity.ok().body(this.orderService.getAllOrders());
		
	}
	
	@GetMapping("/v2/{number}/{size}")
	public ResponseEntity<List<OrderDto>> getAllOrdersByPages(@PathVariable("number") int number, @PathVariable("size") int size){
		return ResponseEntity.ok().body(this.orderService.getAllOrdersByPages(number, size));
		
	}
	
	@GetMapping("/guest/{id}")
	public ResponseEntity<List<OrderDto>> getByGuest(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.orderService.getOrdersByGuest(id));
		
	}
		
	@PutMapping("/{id}")
	public ResponseEntity<OrderDto> editorder(@PathVariable("id") long id,@RequestBody OrderDto orderDto){
		return ResponseEntity.ok().body(this.orderService.editOrder(id, orderDto));
	}
	
	
	@PostMapping("/ordersBetween")
	public ResponseEntity<Long> getNumberOfOrdersBetweenDates(@RequestBody DatesDto dates){
		return ResponseEntity.ok().body(this.orderService.getNumberOfReportsFromTo(dates));
		
	}
	
	@PostMapping("/report/totalMoneyStill")
	public ResponseEntity<Long> getTotalMoneyStillOnAllGuests(@RequestBody DatesDto dates){
		return ResponseEntity.ok().body(this.orderService.SumOfMoneyStill(dates));
		
	}
	
	@PostMapping("/report/totalMoneyPaid")
	public ResponseEntity<Long> getTotalMoneyPaidFromALlGuests(@RequestBody DatesDto dates){
		return ResponseEntity.ok().body(this.orderService.sumOfMoneyPaid(dates));
		
	}
	
	@PostMapping("/report/medicineSessions")
	public ResponseEntity<List<MedicineSessionsReport>> getMedicinesSessionsReport(@RequestBody DatesDto dates){
		return ResponseEntity.ok().body(this.orderService.getMedicineSessionsReport(dates));
		
	}
	@PostMapping("/report/medicineSessionsByMonths")
	public ResponseEntity<List<MedicineNumbersByMonths>> getMedicinesSessionsReportByMonths(@RequestBody DatesDto dates){
		return ResponseEntity.ok().body(this.orderService.getMedicineSessionsReportByMonth(dates));
		
	}
	
//	@GetMapping("/byPage")
//	public ResponseEntity<OrderDto>getOrdersByPage(Pageable pageable){
//		Page<Orders> orderList =  this.orderService.getOrdersByPage(pageable);
//	
//		List<OrderDto> OrderDtoList = new ArrayList<>();
//		for (Orders entry :orderList.getContent()) {
//			
//			OrderDtoList.add(new OrderDto(entry));
//		}
//		
//		OrderResponseDto result = new OrderResponseDto();
//		result.setOrderList(OrderDtoList);
//		result.setPage(orderList.getNumber());
//		result.setSize(orderList.getSize());
//		result.setTotalPages(orderList.getTotalPages());
//		return  ResponseEntity.ok().body(result);
//	}

}
