package com.RKSoualApi.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.RKSoualApi.dto.MedicineCategoryDto;
import com.RKSoualApi.service.MedicineCategoryService;

@RequestMapping(value="/api/category")
@Controller
public class MedicineCategoryController {

	@Autowired 
	private MedicineCategoryService categoryService;
	
	@PostMapping
	public ResponseEntity<MedicineCategoryDto> createMedicine(@RequestBody MedicineCategoryDto categoryDto){
		return ResponseEntity.ok().body(this.categoryService.addCategory(categoryDto));
	}
	
	@GetMapping
	public ResponseEntity<List<MedicineCategoryDto>> getAllCatgories(){
		return ResponseEntity.ok().body(this.categoryService.getAllCategories());
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<MedicineCategoryDto> editCatById(@PathVariable("id") Long id, @RequestBody MedicineCategoryDto categoryDto){
		return ResponseEntity.ok().body(this.categoryService.editCatById(id, categoryDto));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<MedicineCategoryDto> getCatById(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.categoryService.getCatById(id));
	}
	
}
