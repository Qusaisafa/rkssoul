package com.RKSoualApi.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.RKSoualApi.dto.DatesDto;
import com.RKSoualApi.dto.GuestDto;
import com.RKSoualApi.dto.MedicineCategoryDto;
import com.RKSoualApi.dto.SpendingCategoryDto;
import com.RKSoualApi.dto.SpendingCategoryReport;
import com.RKSoualApi.dto.SpendingDto;
import com.RKSoualApi.dto.SpendingReport;
import com.RKSoualApi.service.SpendingService;

@Controller
@RequestMapping(value = "/api/spending")
public class SpendingController {

	@Autowired
	private SpendingService spendingService;;

	@PostMapping
	public ResponseEntity<SpendingDto> addSpending(@RequestBody SpendingDto spendingDto) {
		return ResponseEntity.ok().body(this.spendingService.addSpending(spendingDto));
	}

	@GetMapping("/{id}")
	public ResponseEntity<SpendingDto> getSpendingById(@PathVariable("id") Long id) {
		return ResponseEntity.ok().body(this.spendingService.getSpending(id));
	}

	@PostMapping("/betweenDates")
	public ResponseEntity<List<SpendingDto>> getAllSpendingsBetweenDates(@RequestBody DatesDto dates) {
		return ResponseEntity.ok().body(this.spendingService.getAllSpendingBetweenDates(dates));
	}

	@GetMapping("/v2/{number}/{size}")
	public ResponseEntity<List<SpendingDto>> getAllSpendingsByPages(@PathVariable("number") int number,
			@PathVariable("size") int size) {
		return ResponseEntity.ok().body(this.spendingService.getAllSpendingsByPages(number, size));

	}

	@GetMapping("report/category/{category}")
	public ResponseEntity<SpendingReport> getSpendingReportForCategory(@PathVariable("category") String category) {
		return ResponseEntity.ok().body(this.spendingService.getSpendingReportForCategory(category));
	}

	@PutMapping("/{id}")
	public ResponseEntity<SpendingDto> editSpendingId(@PathVariable("id") Long id,
			@RequestBody SpendingDto spendingDto) {
		return ResponseEntity.ok().body(this.spendingService.editSpending(id, spendingDto));
	}

	@PostMapping("/report/total")
	public ResponseEntity<List<SpendingCategoryReport>> getTotalSpendingsReport(@RequestBody DatesDto dates) {
		return ResponseEntity.ok().body(this.spendingService.getSpendingsReports(dates));
	}

	@PostMapping("/report/totalSpendingAmount")
	public ResponseEntity<Double> getTotalSpendingMoneyAmount(@RequestBody DatesDto dates) {
		return ResponseEntity.ok().body(this.spendingService.sumOfTotalMoneySpend(dates));
	}

	@PostMapping("/category")
	public ResponseEntity<SpendingCategoryDto> createCategory(@RequestBody SpendingCategoryDto spendingCategoryDto) {
		return ResponseEntity.ok().body(this.spendingService.createCategory(spendingCategoryDto));
	}

	@GetMapping("/category")
	public ResponseEntity<List<SpendingCategoryDto>> getAllCatgories() {
		return ResponseEntity.ok().body(this.spendingService.getAllCategories());
	}

	@PutMapping("/category/{id}")
	public ResponseEntity<SpendingCategoryDto> editCategory(@PathVariable("id") Long id,
			@RequestBody SpendingCategoryDto spendingCategoryDto) {
		return ResponseEntity.ok().body(this.spendingService.editCat(id, spendingCategoryDto));
	}

	@GetMapping("/category/{id}")
	public ResponseEntity<SpendingCategoryDto> getCatById(@PathVariable("id") Long id) {
		return ResponseEntity.ok().body(this.spendingService.getCatById(id));
	}
}
