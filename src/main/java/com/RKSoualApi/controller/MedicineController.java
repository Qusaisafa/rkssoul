package com.RKSoualApi.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.RKSoualApi.dto.MedicineDto;
import com.RKSoualApi.service.MedicineService;

@RequestMapping(value="/api/medicine")
@Controller
public class MedicineController {

	@Autowired 
	private MedicineService medicineService;
	
	@PostMapping
	public ResponseEntity<MedicineDto> createMedicine(@RequestBody MedicineDto medicineDto){
		return ResponseEntity.ok().body(this.medicineService.createMedicine(medicineDto));
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<MedicineDto> getMedicine(@PathVariable("id") Long id){
		return ResponseEntity.ok().body(this.medicineService.getMedicine(id));
	}
	
	@GetMapping
	public ResponseEntity<List<MedicineDto>> getAllMedicines(){
		return ResponseEntity.ok().body(this.medicineService.getAllMedicines());
		
	}
		
	@PutMapping("/{id}")
	public ResponseEntity<MedicineDto> editMedicine(@PathVariable("id") long id,@RequestBody MedicineDto medicineDto){
		return ResponseEntity.ok().body(this.medicineService.editMedicine(id, medicineDto));
	}
	
	@GetMapping("/category/{id}")
	public ResponseEntity<List<MedicineDto>> getByCategory(@PathVariable("id") long id){
		return ResponseEntity.ok().body(this.medicineService.findByCategory(id));
	}
}
