package com.RKSoualApi.exceptions;

public class OrderNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public OrderNotFoundException() {
		super("Order not found");
	}
	
	public OrderNotFoundException(long id) {
		super("Order not found:"+id);
	}

}
