package com.RKSoualApi.exceptions;

public class UserNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public UserNotFoundException(String username) {
		super("user not found , username " + username);
	}

}