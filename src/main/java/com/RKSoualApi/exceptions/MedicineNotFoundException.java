package com.RKSoualApi.exceptions;

public class MedicineNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MedicineNotFoundException() {
		super("Medicine not found");
	}

}
