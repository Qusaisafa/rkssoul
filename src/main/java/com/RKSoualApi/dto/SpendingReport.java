package com.RKSoualApi.dto;

import java.util.ArrayList;
import java.util.List;

public class SpendingReport {
	
	private List<SpendingDto> spendings = new ArrayList<>();
	private float totalMoneySpend;
	
	
	public SpendingReport() {
		
	}
	
	public SpendingReport(List<SpendingDto> spendings){
		this.setSpendings(spendings);
		float total = 0;
		for(SpendingDto spending: spendings) {
			total+=spending.getAmount();
		}
		this.totalMoneySpend = total;
	}

	public List<SpendingDto> getSpendings() {
		return spendings;
	}

	public void setSpendings(List<SpendingDto> spendings) {
		this.spendings = spendings;
	}

	public float getTotalMoneySpend() {
		return totalMoneySpend;
	}

	public void setTotalMoneySpend(float totalMoneySpend) {
		this.totalMoneySpend = totalMoneySpend;
	}

	
}
