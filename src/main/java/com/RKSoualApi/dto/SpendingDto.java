package com.RKSoualApi.dto;

import java.util.Date;

import com.RKSoualApi.model.Spending;

public class SpendingDto {
	private Long id;
	
	private String name;
	
	private float amount;
	
	private String description;
	
	private SpendingCategoryDto category;
	
	private Date date;
	
	public SpendingDto() {
		
	}
	
	public SpendingDto(Spending spending) {
		this.date = spending.getCreatedDate();
		this.id = spending.getId();
		this.name = spending.getName();
		this.amount = spending.getAmount();
		this.description = spending.getDescription();
		this.category = new SpendingCategoryDto(spending.getCategory());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public SpendingCategoryDto getCategory() {
		return category;
	}

	public void setCategory(SpendingCategoryDto category) {
		this.category = category;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
