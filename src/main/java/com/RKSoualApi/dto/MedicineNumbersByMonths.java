package com.RKSoualApi.dto;

public class MedicineNumbersByMonths {

	private int number;
	
	private int month;
	
	public MedicineNumbersByMonths(int month, int number) {
		this.month = month;
		this.number = number;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}
	
}
