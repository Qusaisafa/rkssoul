package com.RKSoualApi.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GuestMoneyReport {

	private float totalMoneyStill;
	
	private float totalMoneyPaid;
	
	private float totalprice;
	
	private Long guestId;
	
	private String guestName;
	
	private List<OrderMoneyReportDto> orders = new ArrayList<>();
	
	public GuestMoneyReport() {
		
	}

	public float getTotalMoneyStill() {
		return totalMoneyStill;
	}

	public void setTotalMoneyStill(float totalMoneyStill) {
		this.totalMoneyStill = totalMoneyStill;
	}

	public float getTotalMoneyPaid() {
		return totalMoneyPaid;
	}

	public void setTotalMoneyPaid(float totalMoneyPaid) {
		this.totalMoneyPaid = totalMoneyPaid;
	}

	public float getTotalprice() {
		return totalprice;
	}

	public void setTotalprice(float totalprice) {
		this.totalprice = totalprice;
	}

	public List<OrderMoneyReportDto> getOrders() {
		return orders;
	}

	public void setOrders(List<OrderMoneyReportDto> orders) {
		this.orders = orders;
	}

	public Long getGuestId() {
		return guestId;
	}

	public void setGuestId(Long guestId) {
		this.guestId = guestId;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}
	
}
