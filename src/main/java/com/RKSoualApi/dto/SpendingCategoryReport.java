package com.RKSoualApi.dto;

public class SpendingCategoryReport {
	
	private Long categoryId;
	
	private String categoryName;
	
	private float sum;
	
	public SpendingCategoryReport() {
		
	}
	
	public SpendingCategoryReport(Long categoryId, String categoryName, float sum) {
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.sum = sum;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public float getSum() {
		return sum;
	}

	public void setSum(float sum) {
		this.sum = sum;
	}
	

}
