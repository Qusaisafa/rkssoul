package com.RKSoualApi.dto;


import java.util.Date;

import com.RKSoualApi.model.Appointments;


public class AppointmentsDto {

	private Long id;
	
	private String medicineName;
	
	private Long medicineId;
	
	private Date date;
	
	private long orderId;
	
	private String status;
	
	private String note;
	
	private GuestDto guest;
	
	private Float price;
	
	public AppointmentsDto() {
		
	}
	public AppointmentsDto(Appointments app){
		this.id = app.getId();
		this.medicineName = app.getMedicineName();
		this.date = app.getDate();
		this.status = app.getStatus();
		this.orderId = app.getOrders().getId();
		this.medicineId = app.getMedicineId();
		this.note = app.getNote();
		this.guest = new GuestDto(app.getOrders().getGuest());
		this.price = app.getPrice();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMedicineName() {
		return medicineName;
	}
	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}
	public Long getMedicineId() {
		return medicineId;
	}
	public void setMedicineId(Long medicineId) {
		this.medicineId = medicineId;
	}
	public GuestDto getGuest() {
		return guest;
	}
	public void setGuest(GuestDto guest) {
		this.guest = guest;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	
	
}
