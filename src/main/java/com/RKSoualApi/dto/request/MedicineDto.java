package com.RKSoualApi.dto.request;


import com.RKSoualApi.model.Medicine;

public class MedicineDto {
	private Long id;
	
	private String name;
	
	private String category;
	
	private String description;
	
    
    private String medicineStatus;
	
	public MedicineDto() {
		
	}
	
	public MedicineDto(Medicine medicine) {
		this.id = medicine.getId();
		this.name = medicine.getName();
		this.category = medicine.getCategory().getName();
		this.description = medicine.getDescription();
		this.medicineStatus = medicine.getMedicineStatus();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getMedicineStatus() {
		return medicineStatus;
	}

	public void setMedicineStatus(String medicineStatus) {
		this.medicineStatus = medicineStatus;
	}
	
	
}
