package com.RKSoualApi.dto.request;

import java.util.HashSet;
import java.util.Set;

import com.RKSoualApi.model.Guest;
import com.RKSoualApi.model.Medicine;
import com.RKSoualApi.model.Orders;

public class OrderDto {
	
	private Long id;
	
	private String medicineStatus;

	private String moneyStatus;
	
	private String description;
	
	private float totalPrice;
	
	private float moneyPaid;
	
	private float moneyStill;
	
	private Long guestId;
	
	private Set<Long> medicinesIds = new HashSet<>();

	public OrderDto(){
		
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMoneyStatus() {
		return moneyStatus;
	}
	public void setMoneyStatus(String moneyStatus) {
		this.moneyStatus = moneyStatus;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
	public float getMoneyPaid() {
		return moneyPaid;
	}
	public void setMoneyPaid(float moneyPaid) {
		this.moneyPaid = moneyPaid;
	}
	public float getMoneyStill() {
		return moneyStill;
	}
	public void setMoneyStill(float moneyStill) {
		this.moneyStill = moneyStill;
	}
	public String getMedicineStatus() {
		return medicineStatus;
	}
	public void setMedicineStatus(String medicineStatus) {
		this.medicineStatus = medicineStatus;
	}

	public Long getGuestId() {
		return guestId;
	}

	public void setGuestId(Long guestId) {
		this.guestId = guestId;
	}

	public Set<Long> getMedicinesIds() {
		return medicinesIds;
	}

	public void setMedicinesIds(Set<Long> medicinesIds) {
		this.medicinesIds = medicinesIds;
	}
	
}
