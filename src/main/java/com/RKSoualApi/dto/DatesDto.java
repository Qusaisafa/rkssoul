package com.RKSoualApi.dto;

import java.util.Date;

public class DatesDto {
	
	
	private Date start;
	private Date end;

	public DatesDto() {
		
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
	

}
