package com.RKSoualApi.dto;

import java.util.HashSet;
import java.util.Set;

public class OrdersAppointmentsDto {
		
	private Set<AppointmentsDto> appointments = new HashSet<>();
	

	public OrdersAppointmentsDto() {
		
	}

	public Set<AppointmentsDto> getAppointments() {
		return appointments;
	}


	public void setAppointments(Set<AppointmentsDto> appointments) {
		this.appointments = appointments;
	}
	
	
}
