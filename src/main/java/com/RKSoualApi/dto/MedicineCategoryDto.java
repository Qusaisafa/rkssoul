package com.RKSoualApi.dto;

import com.RKSoualApi.model.MedicineCategory;

public class MedicineCategoryDto {

	private Long id;
	
	private String name;
	
	
	public MedicineCategoryDto() {
		
	}

	public MedicineCategoryDto(MedicineCategory category) {
		this.id = category.getId();
		this.name = category.getName();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	
}
