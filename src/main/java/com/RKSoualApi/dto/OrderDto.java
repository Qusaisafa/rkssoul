package com.RKSoualApi.dto;

import java.util.HashSet;
import java.util.Set;
import com.RKSoualApi.model.Appointments;
import com.RKSoualApi.model.Orders;
import com.RKSoualApi.model.OrdersMedicine;
import com.RKSoualApi.model.Payments;

public class OrderDto {
	
	private Long id;
	
	private String medicineStatus;

	private String moneyStatus;
	
	private String description;
	
	private float totalPrice;
	
	private float moneyPaid;
	
	private float moneyStill;
	
	private GuestDto guest;
	
	private Set<OrderMedicineDto> medicines = new HashSet<>();
	
	private Set<AppointmentsDto> appointments = new HashSet<>();

	private Set<PaymentDto> payments = new HashSet<>();
	
	public OrderDto(){
		
	}
	public OrderDto(Orders order){
		this.id =  order.getId();
		this.moneyPaid = order.getMoneyPaid();
		this.moneyStatus = order.getMoneyStatus();
		this.totalPrice = order.getTotalPrice();
		this.moneyStill = order.getMoneyStill();
		this.guest = new GuestDto(order.getGuest());
		this.description = order.getDescription();
		for(OrdersMedicine orderMedicine:order.getOrdersMedicine()) {
			this.medicines.add(new OrderMedicineDto(orderMedicine));
		}
		this.medicineStatus = order.getMedicineStatus();
		for(Appointments app:order.getAppointments()) {
			this.appointments.add(new AppointmentsDto(app));
		}
		for(Payments pay:order.getPayments()) {
			payments.add(new PaymentDto(pay));
		}
		
	}
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMoneyStatus() {
		return moneyStatus;
	}
	public void setMoneyStatus(String moneyStatus) {
		this.moneyStatus = moneyStatus;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}
	public float getMoneyPaid() {
		return moneyPaid;
	}
	public void setMoneyPaid(float moneyPaid) {
		this.moneyPaid = moneyPaid;
	}
	public float getMoneyStill() {
		return moneyStill;
	}
	public void setMoneyStill(float moneyStill) {
		this.moneyStill = moneyStill;
	}
	public String getMedicineStatus() {
		return medicineStatus;
	}
	public void setMedicineStatus(String medicineStatus) {
		this.medicineStatus = medicineStatus;
	}
	public GuestDto getGuest() {
		return guest;
	}
	public void setGuest(GuestDto guest) {
		this.guest = guest;
	}
	public Set<AppointmentsDto> getAppointments() {
		return appointments;
	}
	public void setAppointments(Set<AppointmentsDto> appointments) {
		this.appointments = appointments;
	}
	public Set<OrderMedicineDto> getMedicines() {
		return medicines;
	}
	public void setMedicines(Set<OrderMedicineDto> medicines) {
		this.medicines = medicines;
	}
	public Set<PaymentDto> getPayments() {
		return payments;
	}
	public void setPayments(Set<PaymentDto> payments) {
		this.payments = payments;
	}
	
}
