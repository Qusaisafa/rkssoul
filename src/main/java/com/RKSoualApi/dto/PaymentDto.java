package com.RKSoualApi.dto;

import java.util.Date;

import com.RKSoualApi.model.Payments;

public class PaymentDto {
	
	private Long orderId;
	
	private float moneyAmount;
	
	private Date date;
	
	private String type;
	
	private String status;
	
	public PaymentDto() {
		
	}

	public PaymentDto(Payments payment) {
		this.orderId = payment.getOrders().getId();
		this.date = payment.getCreatedDate();
		this.moneyAmount = payment.getMoney();
		this.type = payment.getType();
		this.status = payment.getStatus();
	}

	public Long getOrderId() {
		return orderId;
	}


	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}


	public float getMoneyAmount() {
		return moneyAmount;
	}

	public void setMoneyAmount(float moneyAmount) {
		this.moneyAmount = moneyAmount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
