package com.RKSoualApi.dto;


import com.RKSoualApi.model.Guest;

public class GuestSimpileDto {

	private Long id;

	private String name;

	private String phone;


	
	public GuestSimpileDto() {
		
	}
	
	public GuestSimpileDto(Guest guest) {
		this.id = guest.getId();
		this.name = guest.getName();
		this.phone = guest.getPhone();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
