package com.RKSoualApi.dto;


import com.RKSoualApi.model.Medicine;
import com.RKSoualApi.model.OrdersMedicine;

public class ResponseOrderMedicineDto {
	private Long MedicineId;
	
	
    private float userPrice;
    
	public ResponseOrderMedicineDto() {
		
	}
	
	public ResponseOrderMedicineDto(Long MedicineId, float price, float userPrice) {
		this.MedicineId = MedicineId;
		this.userPrice = userPrice;
	}
	public ResponseOrderMedicineDto(Medicine medicine, OrdersMedicine ordersMedicine) {
		this.MedicineId = medicine.getId();
		if(ordersMedicine != null) {
			this.userPrice = ordersMedicine.getUserPrice();

		}
	}


	public Long getMedicineId() {
		return MedicineId;
	}

	public void setMedicineId(Long medicineId) {
		MedicineId = medicineId;
	}

	public float getUserPrice() {
		return userPrice;
	}

	public void setUserPrice(float userPrice) {
		this.userPrice = userPrice;
	}

}
