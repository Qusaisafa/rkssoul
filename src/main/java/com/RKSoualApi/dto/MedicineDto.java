package com.RKSoualApi.dto;


import com.RKSoualApi.model.Medicine;
import com.RKSoualApi.model.OrdersMedicine;

public class MedicineDto {
	private Long id;
	
	private String name;
	
	private MedicineCategoryDto category;
	
	private String description;
	
    private float price;
    
    
    private String medicineStatus;
	
	public MedicineDto() {
		
	}
	
	public MedicineDto(Medicine medicine) {
		this.id = medicine.getId();
		this.name = medicine.getName();
		this.setCategory(new MedicineCategoryDto(medicine.getCategory()));
		this.description = medicine.getDescription();
		this.medicineStatus = medicine.getMedicineStatus();
		this.price = medicine.getPrice();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public MedicineCategoryDto getCategory() {
		return category;
	}

	public void setCategory(MedicineCategoryDto category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public String getMedicineStatus() {
		return medicineStatus;
	}

	public void setMedicineStatus(String medicineStatus) {
		this.medicineStatus = medicineStatus;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
