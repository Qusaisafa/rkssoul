package com.RKSoualApi.dto;

import java.util.HashSet;
import java.util.Set;

import com.RKSoualApi.model.Guest;
import com.RKSoualApi.model.Orders;

public class GuestDto {

	private Long id;

	private String name;

	private String phone;

	private String address;

	private String weight;

	private String age; 

	
	public GuestDto() {
		
	}
	
	public GuestDto(Guest guest) {
		this.id = guest.getId();
		this.name = guest.getName();
		this.phone = guest.getPhone();
		this.address = guest.getAddress();
		this.weight = guest.getWeight();
		this.age = guest.getAge();
		//tODO return guest orders
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

}
