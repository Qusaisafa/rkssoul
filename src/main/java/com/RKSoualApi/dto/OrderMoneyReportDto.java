package com.RKSoualApi.dto;

import com.RKSoualApi.model.Orders;

public class OrderMoneyReportDto {

	private Long orderId;
	
	private float price;
	
	private float MoneyStill;
	
	private float MoneyPaid;
	
	public OrderMoneyReportDto() {
		
	}
	public OrderMoneyReportDto(Orders order) {
		this.orderId = order.getId();
		this.MoneyPaid = order.getMoneyPaid();
		this.MoneyStill = order.getMoneyStill();
		this.price = order.getTotalPrice();
		
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getMoneyStill() {
		return MoneyStill;
	}

	public void setMoneyStill(float moneyStill) {
		MoneyStill = moneyStill;
	}

	public float getMoneyPaid() {
		return MoneyPaid;
	}

	public void setMoneyPaid(float moneyPaid) {
		MoneyPaid = moneyPaid;
	}
	
	
}
