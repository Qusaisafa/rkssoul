package com.RKSoualApi.dto;


import com.RKSoualApi.model.Medicine;
import com.RKSoualApi.model.OrdersMedicine;

public class OrderMedicineDto {
	private Long MedicineId;
	
	private String medicineName;
	
	public String getMedicineName() {
		return medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	private float price;
	
    private float userPrice;
    
    private int numberOfSessions;
    
	public OrderMedicineDto() {
		
	}
	
	public OrderMedicineDto(Long MedicineId, float price, float userPrice) {
		this.MedicineId = MedicineId;
		this.userPrice = userPrice;
	}
	public OrderMedicineDto(OrdersMedicine ordersMedicine) {
		this.medicineName = ordersMedicine.getMedicine().getName();
		this.MedicineId = ordersMedicine.getMedicine().getId();
		this.userPrice = ordersMedicine.getUserPrice();
		this.numberOfSessions = ordersMedicine.getNumberOfSessions();
		this.price = ordersMedicine.getMedicine().getPrice();
	}


	public Long getMedicineId() {
		return MedicineId;
	}

	public void setMedicineId(Long medicineId) {
		MedicineId = medicineId;
	}

	public float getUserPrice() {
		return userPrice;
	}

	public void setUserPrice(float userPrice) {
		this.userPrice = userPrice;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getNumberOfSessions() {
		return numberOfSessions;
	}

	public void setNumberOfSessions(int numberOfSessions) {
		this.numberOfSessions = numberOfSessions;
	}

}
